#include <iostream>;
using namespace std;

class BST
{
    int data;
    BST* left;
    BST* right;

public:

    BST()
    {
        data = 0;
        left = NULL;
        right = NULL;
    }

    BST(int value)
    {
        data = value;
        left = NULL;
        right = NULL;
    }

    BST* BSTInsert(BST* root, int value)
    {
        if (root == NULL)
        {
            return new BST(value);
        }

        if (value > root->data)
        {
            root->right = BSTInsert(root->right, value);
        }
        else
        {
            root->left = BSTInsert(root->left, value);
        }

        return root;
    }

    BST* minValueNode(BST* node)
    {
        BST* current = node;

        while (current != NULL && current->left != NULL)
        {
            current = current->left;
        }

        return current;
    }

    BST* BSTDelete(BST* root, int key)
    {
        if (root == NULL)
            return root;

        if (key < root->data)
            root->left = BSTDelete(root->left, key);

        else if (key > root->data)
            root->right = BSTDelete(root->right, key);

        else
        {
            if (root->left == NULL)
            {
                BST* temp = root->right;
                free(root);
                return temp;
            }
            else if (root->right == NULL)
            {
                BST* temp = root->left;
                free(root);
                return temp;
            }

            BST* temp = minValueNode(root->right);

            root->data = temp->data;
            root->right = BSTDelete(root->right, temp->data);
        }
        return root;
    }



    void BSTInorder(BST* root)
    {
        if (root == NULL)
        {
            return;
        }

        BSTInorder(root->left);
        cout << root->data << endl;
        BSTInorder(root->right);
    }

    void BSTPreorder(BST* root)
    {
        if (root == NULL)
        {
            return;
        }

        cout << root->data << endl;
        BSTPreorder(root->left);
        BSTPreorder(root->right);
    }

    void BSTpostorder(BST* root)
    {

        if (root == NULL)
        {
            return;
        }

        BSTpostorder(root->left);
        BSTpostorder(root->right);
        cout << root->data << endl;
    }

};

class AVLTree
{
public:
    int data;
    AVLTree* left;
    AVLTree* right;
    int height;

    AVLTree(int dataTwo)
    {
        data = dataTwo;
        left = NULL;
        right = NULL;
        height = 1;
    }
};

int height(AVLTree* N)
{
    if (N == NULL)
    {
        return 0;
    }

    return N->height;
}

int max(int a, int b)
{
    if (a > b)
    {
        return a;
    }
    else
    {
        return b;
    }
}

AVLTree* newAVLTree(int data)
{
    AVLTree* root = new AVLTree(data);

    return(root);
}


AVLTree* rightRotate(AVLTree* y)
{
    AVLTree* x = y->left;
    AVLTree* T2 = x->right;

    x->right = y;
    y->left = T2;

    y->height = 1 + (max(height(y->left), height(y->right)));
    x->height = 1 + (max(height(x->left), height(x->right)));

    return x;
}

AVLTree* leftRotate(AVLTree* x)
{
    AVLTree* y = x->right;
    AVLTree* T2 = y->left;

    y->left = x;
    x->right = T2;

    x->height = 1 + (max(height(x->left), height(x->right)));
    y->height = 1 + (max(height(y->left), height(y->right)));

    return y;
}

int getBalance(AVLTree* N)
{
    if (N == NULL)
    {
        return 0;
    }
    else
    {
        return (height(N->left) - height(N->right));
    }

}


AVLTree* insertAVL(AVLTree* node, int key)
{

    if (node == NULL)
    {
        return(newAVLTree(key));
    }

    if (key < node->data)
    {
        node->left = insertAVL(node->left, key);
    }
    else if (key > node->data)
    {
        node->right = insertAVL(node->right, key);
    }
    else
    {
        return node;
    }

    node->height = 1 + max(height(node->left), height(node->right));


    int balance = getBalance(node);


    if (balance > 1 && key < node->left->data)
    {
        return rightRotate(node);
    }

    else if (balance < -1 && key > node->right->data)
    {
        return leftRotate(node);
    }


    else if (balance > 1 && key > node->left->data)
    {
        node->left = leftRotate(node->left);
        return rightRotate(node);
    }


    else if (balance < -1 && key < node->right->data)
    {
        node->right = rightRotate(node->right);
        return leftRotate(node);
    }

    else
    {
        return node;
    }
    
}

void preOrder(AVLTree* root)
{
    if (root != NULL)
    {
        cout << root->data << " ";
        preOrder(root->left);
        preOrder(root->right);
    }
}

class RedBlackTree
{
public:

    bool color; //false to Black, a true to Red
    int data;

    RedBlackTree* parent;
    RedBlackTree* left;
    RedBlackTree* right;

    void rightRotation(RedBlackTree** root, RedBlackTree* A) {

        RedBlackTree* B = A->right, * p = A->parent;

        A->right = B->left;
        if (A->right) A->right->parent = A;

        B->left = A;
        B->parent = p;
        A->parent = B;

        if (p)
        {
            if (p->left == A) p->left = B;
            else p->right = B;
        }
        else *root = B;
    }

    void leftRotation(RedBlackTree** root, RedBlackTree* A) {

        RedBlackTree* B = A->left, * p = A->parent;

        A->left = B->right;
        if (A->left) A->left->parent = A;

        B->right = A;
        B->parent = p;
        A->parent = B;

        if (p)
        {
            if (p->left == A) p->left = B;
            else p->right = B;
        }
        else *root = B;
    }

    void insertRedBlackTree(RedBlackTree** root, int key)
    {
        RedBlackTree* nowy_element, * p = NULL, * d, * x, * y;
        nowy_element = (RedBlackTree*)malloc(sizeof(RedBlackTree));
        nowy_element->left = NULL;
        nowy_element->right = NULL;
        nowy_element->parent = NULL;
        nowy_element->data = key;
        nowy_element->color = true;
        d = *root;
        if (!d)
        {
            nowy_element->parent = NULL;
            nowy_element->color = false;
            *root = nowy_element;
        }
        else
        {
            while (d)
            {
                if (d->data > key)
                {
                    p = d;
                    d = d->left;
                }
                else if (d->data < key)
                {
                    p = d;
                    d = d->right;
                }
                else break;
            }
            if (!d)
            {
                if (p->data > key)
                {
                    p->left = nowy_element;
                }
                else
                {
                    p->right = nowy_element;
                }
                nowy_element->parent = p;
            }

            if (p && p->color == true)
            {
                x = nowy_element;
                while (x)
                {
                    p = x->parent;

                    if (p->parent)
                    {
                        if (p->parent->left == p) y = p->parent->right;
                        else if (p->parent->right == p) y = p->parent->left;
                    }
                    else y = NULL;

                    if (y && y->color == true && p->color == true)
                    {
                        p->color = y->color = false;

                        if (p->parent == *root)
                        {
                            break;
                        }

                        p->parent->color = true;

                        if (p)
                        {
                            x = p->parent;
                        }
                    }
                    else
                    {
                        if (p->parent && y == p->parent->right)
                        {
                            if (p->color == true)
                            {
                                if (p->right == x)
                                {
                                    rightRotation(root, p);
                                }

                                leftRotation(root, p->parent);
                                x->parent->color = false;
                                x->parent->right->color = true;
                            }

                        }
                        else if (p->parent && y == p->parent->left)
                        {
                            if (p->color == true)
                            {
                                if (p->left == x)
                                {
                                    leftRotation(root, p);
                                }

                                rightRotation(root, p->parent);
                                x->parent->color = false;
                                x->parent->left->color = true;
                            }
                        }
                        break;
                    }
                }
            }
        }
    }

    RedBlackTree* deleteKey(RedBlackTree** root, int key)
    {
        if (*root != NULL)
        {
            if ((*root)->data > key)
            {
                return deleteKey(&(*root)->left, key);
            }
            else
            {
                if ((*root)->data < key)
                {
                    return deleteKey(&(*root)->right, key);
                }
                else
                {
                    RedBlackTree* z = *root, * p = z, * r = z, * y;
                    if (z->left == NULL)
                    {
                        if (z->right)
                        {
                            z->right->parent = (*root)->parent;
                        }

                        *root = z->right;
                        y = z->parent;
                        free(z);
                        z = NULL;
                        return y;
                    }
                    else
                    {
                        if (z->right == NULL)
                        {
                            z->left->parent = (*root)->parent;
                            *root = z->left;
                            y = z->parent;
                            free(z);
                            z = NULL;
                            return y;
                        }
                        else
                        {
                            p = z->left;
                            r = z;
                            while (p->right != NULL)
                            {
                                r = p;
                                p = p->right;
                            }

                            int temp = p->data;
                            p->data = z->data;
                            z->data = temp;

                            if (r->data != z->data)
                            {
                                r->right = p->left;
                                if (r->right != NULL)
                                {
                                    r->right->parent = p->parent;
                                }
                            }
                            else
                            {
                                r->left = p->left;
                                if (r->left != NULL)
                                {
                                    r->left->parent = p->parent;
                                }

                            }

                            y = p->parent;
                            free(p);
                            p = NULL;
                            return y;
                        }
                    }
                }
            }
        }
    }


    void removeRedBlackTree(RedBlackTree** root, int key) {

        if (*root != NULL)
        {
            RedBlackTree* q = NULL, * x = NULL, * p = NULL, * y = NULL;
            q = deleteKey(root, key);

            if (*root && q)
            {
                if (q->color == 'R' && q->left && q->left->color == 'R')
                {
                    q = q->left;
                }
                else if (q->color == 'R' && q->right && q->right->color == 'R')
                {
                    q = q->right;
                }

                x = q;
                p = x->parent;
                if (p && p->color == 'R')
                {

                    while (x)
                    {
                        p = x->parent;

                        if (p->parent)
                        {
                            if (p->parent->left == p)
                            {
                                y = p->parent->right;
                            }
                            else if (p->parent->right == p)
                            {
                                y = p->parent->left;
                            }
                        }
                        else
                        {
                            y = NULL;
                        }

                        if (y && y->color == 'R' && p->color == 'R')
                        {
                            p->color = y->color = 'B';
                            if (p->parent == *root) break;

                            p->parent->color = 'R';
                            if (p)x = p->parent;
                        }
                        else
                        {
                            if (p->parent && y == p->parent->right)
                            {
                                if (p->color == 'R')
                                {
                                    if (p->right == x)
                                        rightRotation(root, p);

                                    leftRotation(root, p->parent);
                                    x->parent->color = 'B';
                                    x->parent->right->color = 'R';
                                }
                            }
                            else if (p->parent && y == p->parent->left)
                            {
                                if (p->color == 'R')
                                {
                                    if (p->left == x)
                                    {
                                        leftRotation(root, p);
                                    }


                                    rightRotation(root, p->parent);
                                    x->parent->color = 'B';
                                    x->parent->left->color = 'R';
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
    }
};